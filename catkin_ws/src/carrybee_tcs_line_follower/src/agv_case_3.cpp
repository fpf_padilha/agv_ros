// Copyright 2006-2016 Coppelia Robotics GmbH. All rights reserved. 
// marc@coppeliarobotics.com
// www.coppeliarobotics.com
// 
// -------------------------------------------------------------------
// THIS FILE IS DISTRIBUTED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED
// WARRANTY. THE USER WILL USE IT AT HIS/HER OWN RISK. THE ORIGINAL
// AUTHORS AND COPPELIA ROBOTICS GMBH WILL NOT BE LIABLE FOR DATA LOSS,
// DAMAGES, LOSS OF PROFITS OR ANY OTHER KIND OF LOSS WHILE USING OR
// MISUSING THIS SOFTWARE.
// 
// You are free to use/modify/distribute this file for whatever purpose!
// -------------------------------------------------------------------
//
// This file was automatically created for V-REP release V3.3.2 on August 29th 2016

#include <stdio.h>
#include <stdlib.h>
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Int32.h"
#include "geometry_msgs/Twist.h"

// Global variables (also modified by the topic subscriber):
int tagCounter = 0;
int numSteps = 28;
bool newTag = true;
bool obstacleDetected = false;
bool newLinePos = true;
bool lineLost = false;
double linePosition = 0.5;
//             steps      0    1     2       3      4     5     6    7     8       9    10    11    12    13   14/0    1     2     3     4      5     6     7     8      9    10    11    12    13    14/0    
//             tags      12    1     2       3      4     4     3    5     6       7     8     9    10    11    12     1     2     3     4      4     3     5     6      7     8     9    10    11     12
//double speeds[]    = {30.0, 15.0, 15.0,  20.0, 15.0,  20.0, 20.0, 15.0, 10.0,  30.0, 20.0, 40.0, 20.0, 40.0, 40.0};
double speeds[]      = {30.0, 15.0, 15.0,  20.0, 15.0,  20.0, 20.0, 15.0, 10.0,  30.0, 20.0, 50.0, 20.0, 40.0, 30.0, 20.0, 20.0,  20.0, 25.0,  20.0, 20.0, 15.0, 10.0,  30.0, 20.0, 50.0, 20.0, 40.0, 30.0};
double setpoints[]   = {0.50, 0.50, 0.50,  0.50, 0.50,  0.50, 0.66, 0.50, 0.50,  0.50, 0.50, 0.50, 0.50, 0.50, 0.50, 0.50, 0.50,  0.50, 0.50,  0.50, 0.66, 0.50, 0.50,  0.50, 0.50, 0.50, 0.50, 0.50, 0.50};
double protections[] = {0.00, 0.00, 0.00,  0.00, 0.00,  0.00, 0.00, 0.00, 0.00,  0.00, 0.00, 1.50, 0.00, 0.00, 0.00, 0.00, 0.00,  0.00, 0.00,  0.00, 0.00, 0.00, 0.00,  0.00, 0.00, 1.50, 0.00, 0.00, 0.00};
bool hooks[]         = {true, true, true, false, false, true, true, true, false, true, true, true, true, true, true, true, true, false, false, true, true, true, false, true, true, true, true, true, true};

float simulationTime = 0.0;
int simulationState = 0;

// Topic subscriber callbacks:
void tagStateCallback(const std_msgs::Int32& tagState)
{
	newTag = true;
	tagCounter++;
}

void agvLinePosCallback(const std_msgs::Float64& agvLinePos)
{
	newLinePos = true;
	linePosition = agvLinePos.data;
}

void agvLineLostCallback(const std_msgs::Bool& agvLineLost)
{
	lineLost = agvLineLost.data;
}

void obstacleDetectedCallback(const std_msgs::Bool& detection)
{
	obstacleDetected = detection.data;
}

void simulationTimeCallback(const std_msgs::Float32& simTime)
{
	simulationTime = simTime.data;
}

void simulationStateCallback(const std_msgs::Int32& simState)
{
	simulationState = simState.data;
}

// Main code:
int main(int argc,char* argv[])
{
	// The robot motor velocities and the sensor topic names are given in the argument list
	// (when V-REP launches this executable, V-REP will also provide the argument list)
	std::string cmdVelTopic("/cmd_vel");
	std::string pinStateTopic("/agvHook");
	std::string agvLinePosTopic("/agvLinePos");
	std::string agvLineLostTopic("/agvLineLost");
	std::string tagStateTopic("/agvTagState");
	std::string obstacleDetectedTopic("/obstacleDetected");
	std::string protectionZoneTopic("/protectionZone");

	std::string simulationTimeTopic("/simulationTime");
	std::string simulationStateTopic("/simulationState");
	std::string stopSimulationTopic("/stopSimulation");
	std::string startSimulationTopic("/startSimulation");

	// Create a ROS node. The name has a random component: 
	int _argc = 0;
	char** _argv = NULL;
	std::string nodeName("line_follower");
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check()) {
		printf("roscore not found\n");
		return(0);
	}

	ros::NodeHandle node;  
	printf("agvSlam just started with node name %s\n",nodeName.c_str());

	// 1. Let's subscribe to the sensor and simulation time stream
	ros::Subscriber subTagStateSub = node.subscribe(tagStateTopic.c_str(),5,tagStateCallback);
	ros::Subscriber agvLinePosSub = node.subscribe(agvLinePosTopic.c_str(),5,agvLinePosCallback);
	ros::Subscriber agvLineLostSub = node.subscribe(agvLineLostTopic.c_str(),5,agvLineLostCallback);
	ros::Subscriber obstacleDetectedSub = node.subscribe(obstacleDetectedTopic.c_str(),5,obstacleDetectedCallback);
	ros::Subscriber subSimulationTimeSub = node.subscribe(simulationTimeTopic.c_str(),5,simulationTimeCallback);
	ros::Subscriber subSimulationStateSub = node.subscribe(simulationStateTopic.c_str(),5,simulationStateCallback);

	// 2. Let's prepare publishers for the motor speeds:
	ros::Publisher pinStatePub = node.advertise<std_msgs::Bool>(pinStateTopic.c_str(),5);
	ros::Publisher cmdVelPub = node.advertise<geometry_msgs::Twist>(cmdVelTopic.c_str(),5);
	ros::Publisher protectionZonePub = node.advertise<std_msgs::Float64>(protectionZoneTopic.c_str(),5);
	ros::Publisher startSimulationPub = node.advertise<std_msgs::Bool>(startSimulationTopic.c_str(),5,true);
	ros::Publisher stopSimulationPub = node.advertise<std_msgs::Bool>(stopSimulationTopic.c_str(),5,true);

	ros::Rate loop_rate(20);

	std_msgs::Bool msgTrue;
	std_msgs::Bool hook;
	std_msgs::Float64 protection;
	geometry_msgs::Twist cmdVel;
	msgTrue.data = true;
	
	double setpoint;
	double speed;
	double error;
	double pid = 0.0;
	double lastError = 0.0;
	double Kp = 0.75;
	double Kd = 0;

	startSimulationPub.publish(msgTrue);
	sleep(3);

	// 3. Finally we have the control loop:
	while (ros::ok())
	{ // this is the control loop (very simple, just as an example)

		if (newTag)
		{
			newTag = false;

			if(tagCounter == numSteps)
				tagCounter = 0;
	
			ROS_INFO(" tagCounter = %d", tagCounter);
			// update data
			hook.data = hooks[tagCounter];
			std::string hookStr;
			if(hook.data)
				hookStr = "true";
			else
				hookStr = "false";
			ROS_INFO(" hook = %s", hookStr.c_str());

			setpoint = setpoints[tagCounter];
			ROS_INFO(" setpoint = %f", setpoint);

			speed = speeds[tagCounter] / 60.0;
			ROS_INFO(" speed = %f", speeds[tagCounter]);

			protection.data = protections[tagCounter];
			ROS_INFO(" protectionZone = %f", protections[tagCounter]);

			// publish protection zone
			protectionZonePub.publish(protection);

			// publish pin state
			pinStatePub.publish(hook);
		}

		if (lineLost || obstacleDetected)
		{
			cmdVel.linear.x = 0;
			cmdVel.angular.z = 0;

			// publish the motor speed command
			cmdVelPub.publish(cmdVel);
		}
		else if (newLinePos)
		{
			double vL, vR;
			newLinePos = false;

			// pid control
			error = (setpoint - linePosition);
			pid = Kp*error + Kd*(error - lastError);
			lastError = error;
		
			vL = speed * (1.0 - pid);
			vR = speed * (1.0 + pid);
			cmdVel.linear.x = (vL + vR) / 2.0;
			cmdVel.angular.z = (vR - vL) / (2.0 * 0.288);

			// publish the motor speed command
			cmdVelPub.publish(cmdVel);
		}

		// handle ROS messages:
		ros::spinOnce();

		if(simulationState == 0)
			break;
		else
			// sleep a bit:
			loop_rate.sleep();
	}

	stopSimulationPub.publish(msgTrue);

	//ros::shutdown();
	printf("line_follower just ended!\n");
	return(0);
}

