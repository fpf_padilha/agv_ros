//=================================================================================================
// Copyright (c) 2011, Stefan Kohlbrecher, TU Darmstadt
// Copyright (c) 2017, Fernando Padilha Ferreira, UTFPR Curitiba
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Simulation, Systems Optimization and Robotics
//       group, TU Darmstadt nor the names of its contributors may be used to
//       endorse or promote products derived from this software without
//       specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//=================================================================================================

#include <cstdio>
#include "ros/ros.h"
#include "ros/console.h"

#include "nav_msgs/Path.h"
#include "std_msgs/String.h"
#include "std_msgs/Int32.h"

#include <geometry_msgs/Point32.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PolygonStamped.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include "tf/transform_listener.h"

#include <tf/tf.h>

#include <algorithm>

using namespace std;

double comparePosePosition (const geometry_msgs::PoseStamped& t1, const geometry_msgs::PoseStamped& t2) {
  double dx = t1.pose.position.x - t2.pose.position.x;
  double dy = t1.pose.position.y - t2.pose.position.y;
  return sqrt(dx*dx + dy*dy);
}
double comparePoseOrientation (const geometry_msgs::PoseStamped& t1, const geometry_msgs::PoseStamped& t2) {
  double dth = abs(t1.pose.orientation.z - t2.pose.orientation.z);
  return dth;
}

/**
 * @brief Map generation node.
 */
class PathContainer
{
public:
  PathContainer()
  {
    ros::NodeHandle private_nh("~");

    private_nh.param("target_frame_name", p_target_frame_name_, std::string("map"));
    private_nh.param("source_frame_name", p_source_frame_name_, std::string("base_link"));
    private_nh.param("trajectory_update_rate", p_trajectory_update_rate_, 4.0);
    private_nh.param("trajectory_publish_rate", p_trajectory_publish_rate_, 0.25);
    private_nh.param("marker_distance", p_marker_distance_, 3.0);

    waitForTf();

    ros::NodeHandle nh;
    sys_cmd_sub_ = nh.subscribe("syscommand", 1, &PathContainer::sysCmdCallback, this);
    tag_state_sub_ = nh.subscribe("agvTagState", 1, &PathContainer::tagStateCallback, this);
    trajectory_pub_ = nh.advertise<nav_msgs::Path>("trajectory", 1, true);
    marker_array_pub_ = nh.advertise<visualization_msgs::MarkerArray>("visualization_marker_array", 1, true);
    agv_footprint_pub_ = nh.advertise<geometry_msgs::PolygonStamped>("footprint", 1, true);

    last_reset_time_ = ros::Time::now();

    update_trajectory_timer_ = private_nh.createTimer(ros::Duration(1.0 / p_trajectory_update_rate_), &PathContainer::trajectoryUpdateTimerCallback, this, false);
    publish_trajectory_timer_ = private_nh.createTimer(ros::Duration(1.0 / p_trajectory_publish_rate_), &PathContainer::publishTrajectoryTimerCallback, this, false);

    pose_source_.pose.orientation.w = 1.0;
    pose_source_.header.frame_id = p_source_frame_name_;

    trajectory_.header.frame_id = p_target_frame_name_;
  
    geometry_msgs::Point32 pts[4];
    pts[0].x = 0.824;
    pts[0].y = 0.178;
    pts[1].x = 0.824;
    pts[1].y = -0.178;
    pts[2].x = -0.824;
    pts[2].y = -0.178;
    pts[3].x = -0.824;
    pts[3].y = 0.178;

    agv_footprint_.header.frame_id = p_source_frame_name_; 
    agv_footprint_.polygon.points.push_back(pts[0]);
    agv_footprint_.polygon.points.push_back(pts[1]);
    agv_footprint_.polygon.points.push_back(pts[2]);
    agv_footprint_.polygon.points.push_back(pts[3]);

    distance = 0;
    id_counter = 0;
  }

  void waitForTf()
  {
    ros::Time start = ros::Time::now();
    ROS_INFO("Waiting for tf transform data between frames %s and %s to become available", p_target_frame_name_.c_str(), p_source_frame_name_.c_str() );

    bool transform_successful = false;

    while (!transform_successful){
      transform_successful = tf_.canTransform(p_target_frame_name_, p_source_frame_name_, ros::Time());
      if (transform_successful) break;

      ros::Time now = ros::Time::now();

      if ((now-start).toSec() > 20.0){
        ROS_WARN_ONCE("No transform between frames %s and %s available after %f seconds of waiting. This warning only prints once.", p_target_frame_name_.c_str(), p_source_frame_name_.c_str(), (now-start).toSec());
      }
      
      if (!ros::ok()) return;
      ros::WallDuration(1.0).sleep();
    }

    ros::Time end = ros::Time::now();
    ROS_INFO("Finished waiting for tf, waited %f seconds", (end-start).toSec());
  }


  void sysCmdCallback(const std_msgs::String& sys_cmd)
  {
    if (sys_cmd.data == "reset")
    {
      last_reset_time_ = ros::Time::now();
      trajectory_.poses.clear();
      trajectory_.header.stamp = ros::Time::now();
    }
  }

  void tagStateCallback(const std_msgs::Int32& tagState)
  {
    pose_source_.header.stamp = ros::Time(0);    
    geometry_msgs::PoseStamped pose_out;
    tf_.transformPose(p_target_frame_name_, pose_source_, pose_out);
    addMarker(pose_out.pose, true);
    distance = 0;
  }

  void addMarker(geometry_msgs::Pose pose, bool is_a_tag=false)
  {
    visualization_msgs::Marker marker;
    marker.header.frame_id = std::string("/") + p_target_frame_name_;
    marker.header.stamp = ros::Time();
    marker.ns = "agv_checkpoints";
    marker.id = id_counter++;
    marker.type = visualization_msgs::Marker::ARROW;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = pose.position.x;
    marker.pose.position.y = pose.position.y;
    marker.pose.position.z = pose.position.z;
    marker.pose.orientation.x = pose.orientation.x;
    marker.pose.orientation.y = pose.orientation.y;
    marker.pose.orientation.z = pose.orientation.z;
    marker.pose.orientation.w = pose.orientation.w;
    marker.scale.x = 1.0;
    marker.scale.y = 0.2;
    marker.scale.z = 0.2;
    if(is_a_tag) {
      marker.color.r = 0.0;
      marker.color.g = 0.0;
      marker.color.b = 0.0;
    } else {
      marker.color.r = 1.0;
      marker.color.g = 0.0;
      marker.color.b = 0.0;
    }
    marker.color.a = 1.0; // Don't forget to set the alpha!

    marker_array_.markers.push_back(marker);
    marker_array_pub_.publish(marker_array_);
  }

  void addCurrentTfPoseToTrajectory()
  {
    pose_source_.header.stamp = ros::Time(0);

    geometry_msgs::PoseStamped pose_out;
    geometry_msgs::PoseStamped last_pose;
    geometry_msgs::PoseStamped first_pose;

    tf_.transformPose(p_target_frame_name_, pose_source_, pose_out);

    if (trajectory_.poses.size() != 0){
      first_pose = trajectory_.poses.front();
      last_pose = trajectory_.poses.back();

      //check for loop closure
      if (id_counter > 0 && comparePosePosition(first_pose, pose_out) < 0.1 && comparePoseOrientation(first_pose, pose_out) < 0.17) {
        update_trajectory_timer_.stop();
        ROS_INFO("Loop closure found, stopping trajectory server.");
      }
      //Only add pose to trajectory if it's not already stored
      if (pose_out.header.stamp != last_pose.header.stamp){
        distance += comparePosePosition(last_pose, pose_out);;
        if(distance > p_marker_distance_) {
          addMarker(pose_out.pose);
          distance = 0;
        }

        trajectory_.poses.push_back(pose_out);
      }
    } else{
      trajectory_.poses.push_back(pose_out);
    }

    trajectory_.header.stamp = pose_out.header.stamp;
  }

  void trajectoryUpdateTimerCallback(const ros::TimerEvent& event)
  {

    try{
      addCurrentTfPoseToTrajectory();

      agv_footprint_.header.stamp = ros::Time::now();
      agv_footprint_pub_.publish(agv_footprint_);
    }catch(tf::TransformException e)
    {
      ROS_WARN("Trajectory Server: Transform from %s to %s failed: %s \n", p_target_frame_name_.c_str(), pose_source_.header.frame_id.c_str(), e.what() );
    }
  }

  void publishTrajectoryTimerCallback(const ros::TimerEvent& event)
  {
    trajectory_pub_.publish(trajectory_);
  }

  //parameters
  std::string p_target_frame_name_;
  std::string p_source_frame_name_;
  double p_trajectory_update_rate_;
  double p_trajectory_publish_rate_;
  double p_marker_distance_;

  // Zero pose used for transformation to target_frame.
  geometry_msgs::PoseStamped pose_source_;

  ros::Timer update_trajectory_timer_;
  ros::Timer publish_trajectory_timer_;

  ros::Subscriber tag_state_sub_;
  ros::Subscriber sys_cmd_sub_;
  ros::Publisher  trajectory_pub_;
  ros::Publisher  marker_array_pub_;
  ros::Publisher  agv_footprint_pub_;

  nav_msgs::Path trajectory_;

  tf::TransformListener tf_;

  ros::Time last_reset_time_;
  ros::Time last_pose_save_time_;

  //markers
  geometry_msgs::PolygonStamped agv_footprint_;
  visualization_msgs::MarkerArray marker_array_;
  double distance;
  int id_counter;
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "carrybee_tcs_trajectory_server");

  PathContainer pc;

  ros::spin();

  return 0;
}
